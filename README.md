# GST Magento Plugin for India #

India has GST from July 1. This is a Magento Plugin licensed under MIT license.

## Features ##
- Standard Magento 1.x plugin
- Works in any state in India with appropriate CGST, SGST and IGST taxes
- Installs appropriate tax classes
- Works in price inclusive and and price exclusive modes
- If your Invoice template was well written the taxes will appear correctly.
- Taxes are attributed to SGST, CGST and IGST appropriately and in standard Magento way. Easy to make reports.
## Limitations ##
- Does not dynamically change tax on sale price. So, if your product is to have a lower tax due to a sale price, this will not be calculated. You may have to manually change the tax class.
- Does not generate reports for filing
- Does not work on Magento 2.x. Call us if you need help with M2
- V1 does not support upload of HSN to Tax class map.
- V1 ships with MH, GJ, KA and TN rates. Call us if your home state is other than these.
## How it works ##
- When you install the plugin a new attribute in the “default” Attribute set call HSN is created
- Each product is then saved with a default empty value
- Upon choosing your home state, price rules and rates are installed
- You need a map of product SKU and HSN.  We allow a csv upload with wildcard for sku. (Sample available in admin panel)
- You then have to upload the HSN to GST rate csv. Only have the rates for the HSN that matter to you.

The plugin goes through the products and assigns the correct tax class.
 
## Installation Instructions ##
- Test on a test server with all data. DO NOT TRY ON LIVE SERVER.
- Put server is maintenance mode. IMPORTANT! 
- Unzip the “rrap_gst_v1” file in your magento root
- **IMPORTANT!** From the shell
    $ cd magento_root
    $ php index.php
- This step may take 10 minutes or more depending on the number of products
- Each product is loaded and saved making it a very slow script
- Ensure as much memory as possible is specified in php.ini
- Remove maintenance flag
- Login to admin panel and select “GST” from top menu
- Select your home state
- A new menu will be available – SKU HSN Map. Select this menu and upload your sku csv map.
A sample file is available from the admin panel.
SKU can be a regular expression – so “^SNT.*” matches anything starting with “SNT”.
Check php preg_match documentation.
- Licensed under MIT Open source license.  A git repo will be published soon.
- Warranty : No implied or explicit warranty
- Support : Best effort